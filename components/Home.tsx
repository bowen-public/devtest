import { FunctionComponent } from "react";
import Link from "next/link";
import styles from "./Home.module.scss";

const Home: FunctionComponent = () => {
  return (
    <div className={styles.home}>
      <h1>Dev Test</h1>
      <Link href="/q1">
        <a>Question 1</a>
      </Link>
      <Link href="/q2">
        <a>Question 2</a>
      </Link>
    </div>
  );
};

export default Home;
