import { FunctionComponent, useRef, useState, useEffect } from "react";
import styles from "./Q1.module.scss";
import Link from "next/link";

const Q1: FunctionComponent = () => {
  const [val, setVal] = useState("");

  return (
    <div className={styles.q1}>
      <Instructions />
      <Link href="/">
        <a className={styles.homeLink}>HOME</a>
      </Link>
      <input type="text" />
      <label>regular value</label>
      <div>{/* display the value here */}</div>
      <label>exact timed sequence delayed by 3 seconds</label>
      <div>{/* display the value with a 3 second delay here */}</div>
    </div>
  );
};

export default Q1;

function useDelayedValue<T>(val: T, delay: number): T {
  const [valDelayed, setValDelayed] = useState<T>(val);
  const delayRef = useRef(delay);
  const timerRef = useRef(new Map<NodeJS.Timeout, NodeJS.Timeout>([]));

  return valDelayed;
}

export const Instructions: FunctionComponent = () => {
  return (
    <div className={styles.instructions}>
      <h1>Question 1</h1>
      <ol>
        <li>
          Setup the input to be a controlled input, display the value in the
          first div.
        </li>
        <li>
          Write the hook <code>useDelayedValue</code> {"("}started for you{")"}{" "}
          to take the value as an argument and produce a delayed value of 3
          seconds. Make sure to preserve the exact timing of each letter typed,
          this should NOT be debounced or throttled. For example: If I type
          letter <code>a</code> at 0 seconds, letter <code>b</code> at 1 second,
          and letter <code>c</code> at 2 seconds, then delayed letter{" "}
          <code>a</code> should be displayed at 3 seconds, delayed letter{" "}
          <code>b</code> should be displayed at 4 seconds, and delayed letter{" "}
          <code>c</code> should be displayed at 5 seconds.
          <ul>
            <li>
              Do not use the <code>delay</code> argument in the dependency array
              of the useEffect you will be writing, but rather as a ref. Make
              sure the ref is properly updated if it ends up changing.
            </li>
            <li>
              For this step do NOT use any kind of a cleanup function and ignore{" "}
              <code>timerRef</code>
            </li>
          </ul>
        </li>
        <li>
          Open the console logs in your browser. Start typing into the input,
          and before 3 seconds are up, click the HOME link. You will notice a
          console error{" "}
          <strong>
            Warning: Can{"'"}t perform a React state update on an unmounted
            component. This is a no-op, but it indicates a memory leak in your
            application. To fix, cancel all subscriptions and asynchronous tasks
            in a useEffect cleanup function.
          </strong>{" "}
          <br />
          <br />
          <ul>
            <li>
              Write code that fixes this error, while still making sure that the
              entire timing of the typing sequence is preserved.
            </li>
            <li>
              Use the <code>timerRef</code> as a record of all uncleared
              timeouts so that when the entire hook unmounts you can clear the
              remaining timeouts. The key and the value of each map item should
              be the timeout ID as specified in the types. This will enable you
              to look up and item by id, or simply invoke the{" "}
              <code>forEach</code> method on the map. This map reference should
              ONLY contain all timeouts that have NOT been cleared after the
              setTimeout runs.
            </li>
          </ul>
        </li>
      </ol>
    </div>
  );
};
