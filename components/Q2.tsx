import { FunctionComponent, useRef, useState, useEffect } from "react";
import styles from "./Q2.module.scss";
import Link from "next/link";

const Q2: FunctionComponent = () => {
  const ref = useClickOnOutside<HTMLDivElement>(() => {
    console.log("clicked on outside of element");
  });

  return (
    <div className={styles.q2}>
      <Instructions />
      <Link href="/">
        <a className={styles.homeLink}>HOME</a>
      </Link>
      <div ref={ref} className={styles.box}>
        click outside of me to run the handler
      </div>
    </div>
  );
};

export default Q2;

function useClickOnOutside<
  E extends HTMLElement,
  T extends () => void = () => void
>(handler: T) {
  const elementRef = useRef<E>(null);
  const handlerRef = useRef<T>(handler);

  return elementRef;
}

export const Instructions: FunctionComponent = () => {
  return (
    <div className={styles.instructions}>
      <h1>Question 2</h1>
      <ol>
        <li>
          Create a hook that calls a function everytime a user clicks outside an
          element called useClickOnOutside. Use the function below the component
          as a starting point.
        </li>
        <li>
          Make sure that if the `handler` function changes that it does not
          trigger anything. Also make sure the there are no dependency array
          warnings
        </li>
        <li>
          Make sure to use a cleanup function to get rid of any event listeners
          created
        </li>
      </ol>
    </div>
  );
};
