import { AppProps } from "next/app";
import { FC } from "react";
import "styles/index.scss";

const CustomApp: FC<AppProps> = (appProps) => {
  const { Component, pageProps } = appProps;

  return <Component {...pageProps} />;
};

export default CustomApp;
